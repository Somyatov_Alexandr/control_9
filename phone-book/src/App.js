import React, {Component, Fragment} from 'react';
import './App.css';
import PhoneBook from "./containers/PhoneBook/PhoneBook";
import {Route, Switch} from "react-router-dom";
import PhoneAdd from "./containers/PhoneAdd/PhoneAdd";
import Header from "./components/Header/Header";

class App extends Component {
  render() {
    return (
        <Fragment>
          <Header/>
          <Switch>
            <Route path="/" exact component={PhoneBook}/>
            <Route path="/add-phone" exact component={PhoneAdd}/>
          </Switch>
        </Fragment>
    );
  }
}

export default App;

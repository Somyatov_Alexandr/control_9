import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://homework-abd5f.firebaseio.com/'
});

export default instance;
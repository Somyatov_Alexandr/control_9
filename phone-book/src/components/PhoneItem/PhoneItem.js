import React from 'react';
import './PhoneItem.css';

const PhoneItem = (props) => {
  return (
      <div className="phone__item" onClick={props.showFull}>
        <div className="phone__img-wrap">
          {(props.photo) ? <img src={props.photo} alt={props.username}/> : 'No-image'}
        </div>
        <div className="phone__name">
          {props.username}
        </div>
      </div>
  );
};

export default PhoneItem;

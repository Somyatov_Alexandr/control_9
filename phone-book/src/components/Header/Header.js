import React from 'react';
import {Link} from "react-router-dom";

const Header = () => {
  return (
      <header className="header">
        <div className="container">
          <h1 className="heading">Contacts</h1>
          <Link to="/add-phone" className="phone__add btn">Add new contact</Link>
        </div>
      </header>
  );
};

export default Header;

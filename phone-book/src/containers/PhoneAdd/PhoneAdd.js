import React, {Component} from 'react';
import {Link} from "react-router-dom";
import './PhoneAdd.css';

import axios from '../../axios.phones';

class PhoneAdd extends Component {
  state = {
    username: '',
    phone: '',
    email: '',
    photo: '',
    loading: false
  };

  valueChanged = (event) => {
    const name = event.target.name;
    this.setState({[name]: event.target.value});
  };

  phoneHandler = (event) => {
    event.preventDefault();
    this.setState({loading: true});
    const phoneBook = {
      username: this.state.username,
      phone: this.state.phone,
      email: this.state.email,
      photo: this.state.photo
    };
    axios.post('/phoneBook.json', phoneBook).finally(() => {
      this.setState({loading: false});
      this.props.history.push('/');
    });
  };


  render() {
    const previewImage = (!this.state.photo) ? <div className="no-image">No image</div> : <img src={this.state.photo} alt={this.state.username}/>;
    return (
        <div className="phone__add-page">
          <div className="container">
            <h2 className="add-page__heading heading">Add new contact</h2>
            <form onSubmit={this.phoneHandler}>
              <div className="form-group">
                <label htmlFor="username">Name</label>
                <input type="text" onChange={this.valueChanged} id="username" name="username" className="form-control" required/>
              </div>
              <div className="form-group">
                <label htmlFor="phone">Phone</label>
                <input type="text" onChange={this.valueChanged} id="phone" name="phone" className="form-control" required/>
              </div>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input type="email" onChange={this.valueChanged} id="email" name="email" className="form-control" required/>
              </div>
              <div className="form-group">
                <label htmlFor="email">Photo</label>
                <input type="text" onChange={this.valueChanged} id="photo" name="photo" className="form-control" required/>
              </div>
              <div className="form-group preview-img-wrap">
                <div className="preview__heading">
                  Photo preview
                </div>
                <div className="photo-preview">
                  {previewImage}
                </div>
              </div>
              <div className="form-group">
                <button className="btn">Save</button>
                <Link to="/" className="btn btn-back">Back to contacts</Link>
              </div>
            </form>
          </div>
        </div>
    );
  }
}

export default PhoneAdd;

import React, {Component, Fragment} from 'react';
import PhoneItem from "../../components/PhoneItem/PhoneItem";
import './PhoneList.css';
import {fetchContacts, showFullContact} from "../../store/action";
import {connect} from "react-redux";
import Modal from "../../components/UI/Modal/Modal";
import FullContact from "../FullContact/FullContact";

class PhoneList extends Component {
  state = {
    fullShow: false
  };

  componentDidMount () {
    this.props.fetchContact();
  }

  fullShowHandler = () => {
    this.setState({fullShow: true});
  };

  fullShowCancelHandler = () => {
    this.setState({fullShow: false});
  };



  render() {
    const phoneList = this.props.phoneBook;
    const phoneItem = Object.keys(this.props.phoneBook).map(id =>(
      <PhoneItem
          key={id}
          username={phoneList[id].username}
          photo={phoneList[id].photo}
          showFull={this.fullShowHandler}
          clicked={this.props.onShowFullContact(id)}
      />
    ));

    return (
        <Fragment>
          <div className="phone__list">
            <div className="container">
              {phoneItem}
            </div>
          </div>
          <Modal show={this.state.fullShow} closed={this.fullShowCancelHandler}>
            <FullContact/>
          </Modal>
        </Fragment>
    );
  }
}
const mapStateToProps = state => {
  return {
    phoneBook: state.phoneBook
  }
};

const mapDispatchToProps = dispatch => {
  return {
    fetchContact: () => dispatch(fetchContacts()),
    onShowFullContact: id => dispatch(showFullContact(id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(PhoneList);

import React, {Component} from 'react';
import {Link} from "react-router-dom";
import './FullContact.css';

class FullContact extends Component {
  render() {
    return (
        <div className="contact">
          <div className="contact__info-wrap">
            <div className="contact__img-wrap">
              <img src="https://vignette.wikia.nocookie.net/uncyclopedia/images/0/09/Andrew_Hippy.jpg/revision/latest?cb=20110318102413" alt="test"/>
            </div>
            <div className="contact__info">
              <h3 className="contact__heading heading">
                John Smith
              </h3>
              <div className="contact__phone">
                +996 555 55 55 55
              </div>
              <div className="contact__email">
                <a href="mailto:alex.som86@gmail.com">alex.som86@gmail.com</a>
              </div>
            </div>
          </div>
          <div className="contact__controls">
            <Link to="/edit" className="btn">Edit</Link>
            <button className="btn btn-back">Delete</button>
          </div>
        </div>
    );
  }
}

export default FullContact;

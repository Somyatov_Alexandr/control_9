import React, {Component} from 'react';
import PhoneList from "../PhoneList/PhoneList";
import './PhoneBook.css';

class PhoneBook extends Component {
  render() {
    return (
        <div className="phone">
          <PhoneList/>
        </div>
    );
  }
}
export default PhoneBook;

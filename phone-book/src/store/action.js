import * as actionTypes from "./actionTypes";
import axios from '../axios.phones';

export const fetchContactRequest = () => {
  return { type: actionTypes.FETCH_CONTACT_REQUEST };
};

export const fetchContactSuccess = phoneBook => {
  return { type: actionTypes.FETCH_CONTACT_SUCCESS, phoneBook};
};

export const fetchContactError = () => {
  return { type: actionTypes.FETCH_CONTACT_ERROR };
};

export const showFullContact = id => {
  return { type: actionTypes.SHOW_FULL_CONTACT, id};
};

export const fetchContacts = () => {
  return dispatch => {
    dispatch(fetchContactRequest());
    axios.get('/phoneBook.json').then(response => {
      dispatch(fetchContactSuccess(response.data));
    }, error => {
      dispatch(fetchContactError());
    });
  }
};


import * as actionTypes from './actionTypes';
const initialState = {
  phoneBook: {}
};


const reducer = (state = initialState, action) => {
  switch (action.type){
    case actionTypes.FETCH_CONTACT_SUCCESS:
      return {
          ...state,
        phoneBook: action.phoneBook
      };
    case actionTypes.SHOW_FULL_CONTACT:
      console.log(action.id);
      return {
          ...state,
        id: action.id
      };
    default:
      return state;
  }
};

export default reducer;